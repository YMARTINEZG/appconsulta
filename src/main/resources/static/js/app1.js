var app = angular.module('myApp', []);

app.controller('myController', function($scope, $http) {
  var mock1 = {
    "resumen":
        [
                {
                   "systemid":1,
                   "description":"CAJA",
                   "lastmatch": 10,
                   "previousmatch": 7
                },
                {
                   "systemid":5,
                   "description":"CAJA5",
                   "lastmatch": 17,
                   "previousmatch": 8
                },
                {
                   "systemid":2,
                   "description":"TRAGAMONEDA",
                   "lastmatch": 15,
                   "previousmatch": 17
                }
        ]
  };
  
  //data = localhost:8080/all/resumen
  
  var urlBase="http://localhost:8080";
  $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
  $http.get(urlBase + '/all/resumen').
  success(function(data) {
          console.log(data);
          $scope.ocw.resumen = data;
          console.log($scope.ocw.resumen);
  });		
  
  //$scope.ocw = mock1; 
  
});
