package com.ryg.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;


@Data
@Entity
@Table(name="razon_social")
public class RazonSocial implements Serializable{
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @Column(name="grupo_id")
    private int grupoId;
    
    @Column(name="nombre")
    private String nombre;
    
    @Column(name="ruc")
    private int ruc;
    
}
