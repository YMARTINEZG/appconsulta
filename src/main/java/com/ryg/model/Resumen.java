
package com.ryg.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="resumen")
public class Resumen implements Serializable{
    
    @Id
    @Column(name="systemid")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @Column(name="description")
    private String description;
    
    @Column(name="lastmatch")
    private int lastmatch;  
    
    @Column(name="previousmatch")
    private int previousmatch;  
/*    
    public Resumen(String description, int lastmatch, int previousmatch) {
        this.description = description;
        this.lastmatch = lastmatch;
        this.previousmatch = previousmatch;
    }
*/
    
    
}
