package com.ryg.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name="factura")
public class Factura implements Serializable{
    
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    
    @Column(name="razon_id")
    private int razonId;
    
    @Column(name="monto")
    private double monto;
    
    @Column(name="estado")
    private int estado;
    
    //@Column(name="fecha_venc")
    //private //date fechaVenc;
    
    @Column(name="periodo")
    private String periodo;
    
}
