package com.ryg.exception;

public final class InternalServerException extends RuntimeException {
    public InternalServerException() {
        super();
    }
    public InternalServerException(String message) {
        super(message);
    }
}
