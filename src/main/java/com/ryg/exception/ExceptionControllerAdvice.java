package com.ryg.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionControllerAdvice {
 
//    @ExceptionHandler(Exception.class)
//    public String exception(Exception e) {
//
//        return "error";
//    }
    
//    @ExceptionHandler(Exception.class)
//    @ResponseStatus(HttpStatus.BAD_REQUEST)
//    public @ResponseBody ExceptionInfo exception(Exception ex){
//
//        ExceptionInfo response = new ExceptionInfo();
//        response.setUrl("hola2.com");
//        response.setMessage(ex.getMessage());
//
//        return response;
//    }    
    
    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ExceptionInfo handleException(NotFoundException ex) {
        ExceptionInfo response = new ExceptionInfo();
        response.setMessage(ex.getMessage());        
        return response;
    }   
    
    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    ExceptionInfo handleException(InternalServerException ex) {
        ExceptionInfo response = new ExceptionInfo();
        response.setMessage(ex.getMessage());        
        return response;
    }  
    
    
    
    
           
}
