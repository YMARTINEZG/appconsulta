package com.ryg.controller;

import com.ryg.exception.NotFoundException;
import com.ryg.model.Sala;
import com.ryg.service.ConsultaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/consulta")
public class ConsultaCtrl {

    @Autowired
    ConsultaService consultaService;

    @RequestMapping(value = "/")
    @ResponseBody
    public String home (){
        return "Ingrese id de razon social";
    }
    
    @RequestMapping(value = "/{razonId}",method = RequestMethod.GET)
    @ResponseBody
    public Sala show(@PathVariable int razonId) throws NotFoundException{
        //consultaService.consultaSala(razonId);
        if(consultaService.consultaSala(razonId)==null) throw new NotFoundException("Sala no encontrada");
        else{
            return consultaService.consultaSala(razonId);
        }

    }
    
    @RequestMapping(value = "/{razonId}/alerta",method = RequestMethod.GET)
    @ResponseBody
    public String show2(@PathVariable int razonId){ //throws NotFoundException{
        return consultaService.consultaAlerta(razonId);
    }
}
