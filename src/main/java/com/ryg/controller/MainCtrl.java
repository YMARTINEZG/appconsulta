
package com.ryg.controller;

// @RestController = @Controller + @ResponseBody

import com.ryg.model.Resumen;
import com.ryg.model.Users;
import com.ryg.service.ResumenService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainCtrl {
    
    @Autowired
    ResumenService resumenService;

    // @GetMapping = @RequestMapping + Http GET method
    @GetMapping("/")
    public String home (){
        return "App operativa";
    }
    
    @GetMapping("/resumen")
    public String getAllSumary (){
        return "get all sumary";
    }
    
    @GetMapping("/all/resumen")
    public List<Resumen> getAllResumen (){
        
        return resumenService.getAllResumen();
    }   
      
    @GetMapping("/users")
    public List<Users> getAllUsers (){
        
        System.out.println("controller");
        Users user1 = new Users();
        user1.setId(12);
        user1.setName("Yvan");
        user1.setEmail("yvan@gmail.com");
        
        Users user2 = new Users();
        user2.setId(14);
        user2.setName("Piero");
        user2.setEmail("piero@gmail.com");
        
        List<Users> list = new ArrayList<Users>();
        list.add(user1);
        list.add(user2);
        
        return list;
    } 
    
}

