package com.ryg.controller;

import com.ryg.exception.NotFoundException;
import com.ryg.model.Factura;
import com.ryg.service.FacturaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/factura")
public class FacturaCtrl {
    
    @Autowired
    FacturaService facturaService;
    
    @RequestMapping(value = "/{razonId}/registro",method = RequestMethod.GET)
    @ResponseBody
    public List<Factura> show(@PathVariable int razonId) throws NotFoundException{
        if(facturaService.consultaFactura(razonId)==null) throw new NotFoundException("No tiene facturas registradas");
        return facturaService.consultaFactura(razonId);
    }
    
    @RequestMapping(value = "/{razonId}/deuda",method = RequestMethod.GET)
    @ResponseBody
    public int show2(@PathVariable int razonId){
        return facturaService.consultaDeuda(razonId);
    }
    
    @RequestMapping(value = "/{razonId}/monto",method = RequestMethod.GET)
    @ResponseBody
    public double show3(@PathVariable int razonId){
        return facturaService.consultaMonto(razonId);
    }
}
