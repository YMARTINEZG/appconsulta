package com.ryg.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Qualifier;

public abstract class AbstractDao {
    
    @PersistenceContext(unitName="main")
    @Qualifier(value = "entityManagerFactory")        
    public EntityManager entityManager;
    
    
    protected Session getSession() {
        //return sessionFactory.getCurrentSession();
        //visto en documentacion de hibernate 5.2
        return entityManager.unwrap( Session.class );
    }
 
    public void persist(Object entity) {
        entityManager.persist(entity);
        //getSession().persist(entity);
    }
 
    public void delete(Object entity) {
        entityManager.remove(entity);
        //getSession().delete(entity);
    }
}
