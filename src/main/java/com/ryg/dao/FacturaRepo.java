package com.ryg.dao;

import com.ryg.model.Factura;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface FacturaRepo extends CrudRepository<Factura, Integer>{

    
    @Query("FROM Factura f where f.razonId = :razonId")
    List<Factura> findByRazonId(@Param("razonId") int razonId);
    
    @Query("FROM Factura f where f.estado = :estado")
    List<Factura> findByEstado(@Param("estado") int estado);
}