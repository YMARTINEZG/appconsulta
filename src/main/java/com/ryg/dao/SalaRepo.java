
package com.ryg.dao;

import com.ryg.model.Sala;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface SalaRepo extends CrudRepository<Sala, Integer>{
    
    
    @Query("FROM Sala s where s.razonId = :razonId")
    List<Sala> findByRazonId(@Param("razonId") int razonId);
}
