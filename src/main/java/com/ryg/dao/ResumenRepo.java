
package com.ryg.dao;

import com.ryg.model.Resumen;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ResumenRepo extends CrudRepository<Resumen, Integer>{
    
    @Query("FROM Resumen")
    List<Resumen> gettAllResumen();
}
