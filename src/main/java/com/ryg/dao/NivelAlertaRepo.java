
package com.ryg.dao;

import com.ryg.model.NivelAlerta;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface NivelAlertaRepo extends CrudRepository<NivelAlerta, Integer>{
    
    @Query("FROM NivelAlerta n where n.mesesAtraso >= :resta")
    List<NivelAlerta> findByResta(@Param("resta") int resta);
}
