package com.ryg.dao;

import com.ryg.model.Tolerancia;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ToleranciaRepo extends CrudRepository<Tolerancia, Integer>{
    
    @Query("FROM Tolerancia t where t.grupoId = :grupoId")
    List<Tolerancia> findByGrupoId(@Param("grupoId") int grupoId);
}
