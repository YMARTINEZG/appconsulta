package com.ryg.dao;

import com.ryg.model.RazonSocial;
import org.springframework.data.repository.CrudRepository;

public interface RazonSocialRepo extends CrudRepository<RazonSocial, Integer>{
    
}
