package com.ryg.dao;

import com.ryg.model.Grupo;
import org.springframework.data.repository.CrudRepository;

public interface GrupoRepo extends CrudRepository<Grupo, Integer> {
    
}
