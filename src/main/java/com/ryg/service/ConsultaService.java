package com.ryg.service;

import com.ryg.model.NivelAlerta;
import com.ryg.dao.NivelAlertaRepo;
import com.ryg.model.RazonSocial;
import com.ryg.dao.RazonSocialRepo;
import com.ryg.dao.SalaRepo;
import com.ryg.dao.ToleranciaRepo;
import com.ryg.model.Sala;
import com.ryg.model.Tolerancia;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service("ConsultaService")
@Transactional
public class ConsultaService {
    
    @Autowired
    FacturaService facturaService;
    
    @Autowired
    SalaRepo salaRepo;
    
    @Autowired
    ToleranciaRepo toleranciaRepo;
    
    @Autowired
    RazonSocialRepo razonSocialRepo;
    
    @Autowired
    NivelAlertaRepo nivelAlertaRepo;
    
    public Sala consultaSala(int razonId){
        
        List<Sala> listSala = salaRepo.findByRazonId(razonId);
        Sala consultasala = listSala.get(0);
        if(consultasala == null){
            return null;
        }
        return consultasala;
    }
    
    public String consultaAlerta(int razonId){
        
        RazonSocial razon = razonSocialRepo.findOne(razonId);
        List<Tolerancia> toler = toleranciaRepo.findByGrupoId(razon.getGrupoId());
        NivelAlerta nivel;
        int resta = facturaService.consultaDeuda(razonId)-toler.get(0).getTolerancia();
        //if(resta <= toler.get(0).getMaxAlerta()){
        //    nivel = nivelAlertaRepo.findOne(resta);
        //}else{
        //    nivel = nivelAlertaRepo.findOne(4);
        //}
        if(resta <= 0){
            nivel = nivelAlertaRepo.findOne(1);
        }else if(resta > 0){
            List<NivelAlerta> list = nivelAlertaRepo.findByResta(resta);
            
            
        }
        return "ok";      
       
    }
}
