
package com.ryg.service;

import com.ryg.dao.ResumenRepo;
import com.ryg.model.Resumen;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ResumenService")
@Transactional
public class ResumenService {
    
   @Autowired
   ResumenRepo resumenRepo;
   
   public List<Resumen> getAllResumen(){
       
       List<Resumen> listResumen = resumenRepo.gettAllResumen();
       return listResumen;
       
       //Resumen resu1 = new Resumen(1, "CAJA1", 47, 20);
       //List<Resumen> list1 = new ArrayList<Resumen>();
       //list1.add(resu1);
       //return list1;
    }
    
    
}
