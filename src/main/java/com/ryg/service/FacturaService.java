package com.ryg.service;

import com.ryg.dao.FacturaRepo;
import com.ryg.model.Factura;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("FacturaService")
@Transactional
public class FacturaService {
    
   @Autowired
   FacturaRepo facturaRepo;

    public List<Factura> consultaFactura(int razonId){
       
       List<Factura> listFactura = facturaRepo.findByRazonId(razonId);
        return listFactura;
    }
    
    public int consultaDeuda(int razonId){
        
        int nro = 0;
        List<Factura> listFactura = facturaRepo.findByRazonId(razonId);
        for(int i = 0; i < listFactura.size(); i++){
            if(listFactura.get(i).getEstado()==1){
                nro++;
            }
        }
        return nro;
    }
    
    public double consultaMonto(int razonId){
        
        double monto = 0;
        int estado = 1;
        List<Factura> listFactura = facturaRepo.findByEstado(estado);
        for(int i = 0; i < listFactura.size(); i++){
            if(listFactura.get(i).getRazonId()==razonId) {
                monto = monto + listFactura.get(i).getMonto();
            }
        }
        return monto;
    }
}
